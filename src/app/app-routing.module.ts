import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/estudiantes/login/login.component';
import { HomeComponent } from './components/estudiantes/home/home.component';
import { AuthGuard } from './auth.guard';
import { ProfileComponent } from './components/estudiantes/profile/profile.component';
import { KardexComponent } from './components/estudiantes/kardex/kardex.component';
import { HorarioComponent } from './components/estudiantes/horario/horario.component';
import { EvaldocenteComponent } from './components/estudiantes/evaldocente/evaldocente.component';
import { GuiastrabajoComponent } from './components/estudiantes/guiastrabajo/guiastrabajo.component';
import { SubirtrabajosComponent } from './components/estudiantes/subirtrabajos/subirtrabajos.component';
import { TamizajeComponent } from './components/estudiantes/tamizaje/tamizaje.component';

const routes: Routes = [
  { path: 'estudiantes/inicio-de-sesion', component: LoginComponent },
  { path: 'estudiantes/inicio', component: HomeComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/perfil', component: ProfileComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/kardex', component: KardexComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/horario', component: HorarioComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/evaluacion-docente', component: EvaldocenteComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/guias-de-tabajo', component: GuiastrabajoComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/subir-trabajos', component: SubirtrabajosComponent, canActivate: [AuthGuard], },
  { path: 'estudiantes/cuestionario-de-tamizaje', component: TamizajeComponent, canActivate: [AuthGuard], },
  { path: '', redirectTo: 'estudiantes/inicio-de-sesion', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
