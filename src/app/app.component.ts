import { Component, OnInit } from '@angular/core';

import { AlertasService } from './services/alertas/alertas.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'CBTA190';

  currentMode: boolean = false;
  constructor(private alertasService: AlertasService) {}
  
  ngOnInit(): void {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
    this.alertasService.setDarkMode(this.currentMode);
  }

  
}
