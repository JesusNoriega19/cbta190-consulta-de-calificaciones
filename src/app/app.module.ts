  import { NgModule } from '@angular/core';
  import { BrowserModule } from '@angular/platform-browser';
  import { FormsModule } from '@angular/forms';
  import { AppRoutingModule } from './app-routing.module';
  import { AppComponent } from './app.component';
  import { LoginComponent } from './components/estudiantes/login/login.component';
  import { NavbarComponent } from './components/estudiantes/navbar/navbar.component';
  import { FooterComponent } from './components/estudiantes/footer/footer.component';
  import { HomeComponent } from './components/estudiantes/home/home.component';
  import { KardexComponent } from './components/estudiantes/kardex/kardex.component';
  import { AuthGuard } from './auth.guard'; // Asegúrate de tener un archivo auth.guard.ts
  import { RouterModule } from '@angular/router';
  import { ProfileComponent } from './components/estudiantes/profile/profile.component';
  import { HorarioComponent } from './components/estudiantes/horario/horario.component';
  import { EvaldocenteComponent } from './components/estudiantes/evaldocente/evaldocente.component';
  import { ReactiveFormsModule } from '@angular/forms';
  import { GuiastrabajoComponent } from './components/estudiantes/guiastrabajo/guiastrabajo.component';
  import { SubirtrabajosComponent } from './components/estudiantes/subirtrabajos/subirtrabajos.component';
  import { TamizajeComponent } from './components/estudiantes/tamizaje/tamizaje.component';
  import { HttpClientModule } from '@angular/common/http';


  @NgModule({
    declarations: [
      AppComponent,
      LoginComponent,
      NavbarComponent,
      FooterComponent,
      HomeComponent,
      KardexComponent,
      ProfileComponent,
      HorarioComponent,
      EvaldocenteComponent,
      GuiastrabajoComponent,
      SubirtrabajosComponent,
      TamizajeComponent,
    ],
    imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      RouterModule, // Asegúrate de agregar RouterModule aquí
      ReactiveFormsModule,
      HttpClientModule,
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent]
  })
  export class AppModule { }
