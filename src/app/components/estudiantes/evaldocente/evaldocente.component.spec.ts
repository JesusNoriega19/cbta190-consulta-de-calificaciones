import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaldocenteComponent } from './evaldocente.component';

describe('EvaldocenteComponent', () => {
  let component: EvaldocenteComponent;
  let fixture: ComponentFixture<EvaldocenteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EvaldocenteComponent]
    });
    fixture = TestBed.createComponent(EvaldocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
