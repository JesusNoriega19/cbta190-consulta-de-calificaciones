import { Component, OnInit } from '@angular/core';
import { EvaluacionDocenteService } from '../../../services/evaluacion-docente/evaluacion-docente.service';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';
import { Pregunta, Materia } from '../../../interfaces/evaluacion-docente/evaluacion-docente.interface';
import { AlertasService } from '../../../services/alertas/alertas.service';

@Component({
  selector: 'app-evaldocente',
  templateUrl: './evaldocente.component.html',
  styleUrls: ['./evaldocente.component.css', 'evaldocente-querys.css', 'evaldocente-querys-mobile.css'],
})
export class EvaldocenteComponent implements OnInit {
  preguntas: Pregunta[] = [];
  preguntaActual: number = 0;
  respuestas: Map<string, string> = new Map();
  evaluacionTerminada: boolean = false;
  mostrarInstrucciones: boolean = false;
  currentMode: boolean = false;
  idEstudiante!: number;

  constructor(
    private evaluacionDocenteService: EvaluacionDocenteService,
    private estudiantesService: EstudiantesService,
    private alertasService: AlertasService
  ) {}

  ngOnInit(): void {
    const estudianteAutenticado = this.estudiantesService.getEstudianteAutenticado();

    if (estudianteAutenticado) {
      this.idEstudiante = estudianteAutenticado.id;
      this.preguntas = this.evaluacionDocenteService.getPreguntasPorEstudiante(this.idEstudiante);
    }

    this.verificarEvaluacionRealizada();
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  iniciarEvaluacion() {
    this.mostrarInstrucciones = true;
  }

  seleccionarRespuesta(materia: Materia, respuesta: string) {
    this.respuestas.set(`${this.preguntaActual}_${materia.nombre}`, respuesta);
  }

  cambiarPregunta() {
    const pregActual = this.preguntas[this.preguntaActual];
    const materiasPreguntaActual = pregActual.materias.map(m => m.nombre);
    const respuestasMateria = materiasPreguntaActual.map(materia => this.respuestas.get(`${this.preguntaActual}_${materia}`));

    if (respuestasMateria.every(respuesta => respuesta !== undefined && respuesta !== null)) {
      this.preguntaActual++;
    } else {
      this.alertasService.mostrarAlertaAdvertencia('Debes seleccionar una respuesta por cada profesor para poder avanzar.');
    }
  }

  finalizarEvaluacion() {
    const pregActual = this.preguntas[this.preguntaActual];
    const materiasPreguntaActual = pregActual.materias.map(m => m.nombre);
    const respuestasMateria = materiasPreguntaActual.map(materia => this.respuestas.get(`${this.preguntaActual}_${materia}`));

    if (respuestasMateria.every(respuesta => respuesta !== undefined && respuesta !== null)) {
      this.evaluacionTerminada = true;
      this.guardarEstadoEvaluacion();
    } else {
      this.alertasService.mostrarAlertaAdvertencia('Debes seleccionar una respuesta por cada profesor para poder terminar.');
    }
  }

  getRadioId(pregunta: number, materia: Materia): string {
    return `${pregunta}_${materia.nombre}_${materia.profesor}`;
  }

  getRadioName(pregunta: number, materia: Materia): string {
    return `${pregunta}_${materia.nombre}`;
  }

  verificarEvaluacionRealizada() {
    const evaluacionRealizada = this.evaluacionDocenteService.verificarEvaluacionRealizada(this.idEstudiante);
    if (evaluacionRealizada) {
      this.evaluacionTerminada = true;
    }
  }

  guardarEstadoEvaluacion() {
    this.evaluacionDocenteService.guardarEstadoEvaluacion(this.idEstudiante);
  }

  cancelarEvaluacion() {
    this.mostrarInstrucciones = false;
    this.preguntaActual = 0;
  }
}
