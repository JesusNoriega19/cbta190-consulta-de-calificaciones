import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiastrabajoComponent } from './guiastrabajo.component';

describe('GuiastrabajoComponent', () => {
  let component: GuiastrabajoComponent;
  let fixture: ComponentFixture<GuiastrabajoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiastrabajoComponent]
    });
    fixture = TestBed.createComponent(GuiastrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
