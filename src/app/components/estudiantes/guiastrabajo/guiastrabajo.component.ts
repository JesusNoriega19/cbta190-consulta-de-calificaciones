// guiastrabajo.component.ts
import { Component, OnInit } from '@angular/core';
import { GuiasTrabajoService } from '../../../services/guias-trabajo/guias-trabajo.service';
import { GuiaEstudio } from '../../../interfaces/guias-trabajo/guias-trabajo.interface';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';

@Component({
  selector: 'app-guiastrabajo',
  templateUrl: './guiastrabajo.component.html',
  styleUrls: ['./guiastrabajo.component.css', 'guiastrabajo-querys.css']
})
export class GuiastrabajoComponent implements OnInit {
  periodoSeleccionado: number = 1;
  guiasEstudio: GuiaEstudio[] = [];
  estudiante: any = {};
  currentMode: boolean = false;

  constructor(private guiasTrabajoService: GuiasTrabajoService, private estudiantesService: EstudiantesService) {}

  ngOnInit(): void {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
    this.estudiante = this.estudiantesService.getEstudianteAutenticado();
    this.cargarGuiasEstudio();

  }
  
  cargarGuiasEstudio() {
    // Pasa el id del estudiante al servicio
    const estudianteId = this.estudiante?.id || null; 
    
    this.guiasTrabajoService.getGuiasEstudio(estudianteId)
      .subscribe(guias => this.guiasEstudio = guias);
  }
    

  obtenerRutaDocumento(guia: GuiaEstudio): string {
    const guiaUrl = `../../../assets/guia-documentos/${guia.asignatura}-guia-periodo${this.periodoSeleccionado}.pdf`;
    return this.documentoExiste(guiaUrl) ? guiaUrl : 'no-disponible';
  }

  documentoExiste(ruta: string): boolean {
    const http = new XMLHttpRequest();
    http.open('HEAD', ruta, false);
    http.send();
    return http.status !== 404;
  }

  onPeriodoChange() {
    this.cargarGuiasEstudio();
  }
}
