import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css','./home-querys.css']
})
export class HomeComponent implements OnInit{

  constructor(private router: Router) { }
  navegarAOtroComponente() {
    this.router.navigate(['/estudiantes/kardex']);
  }
  
  currentMode: boolean = false;
  
  ngOnInit(): void {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

}
