import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';
import { horarioEstudiante } from '../../../interfaces/estudiantes/horarioEstudiante.interface';

@Component({
  selector: 'app-horario',
  templateUrl: './horario.component.html',
  styleUrls: ['./horario.component.css', 'horario-querys.css']
})
export class HorarioComponent implements OnInit {

  @ViewChild('tablaHorario') tablaHorario!: ElementRef;

  horario: horarioEstudiante | undefined;
  currentMode: boolean = false;

  constructor(private estudiantesService: EstudiantesService) { }

  ngOnInit(): void {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';

    this.getHorarioEstudiante();
  }

  getHorarioEstudiante() {
    this.horario = this.estudiantesService.getHorarioEstudianteAutenticado();
    console.log('Horario obtenido:', this.horario);
    console.log(this.horario?.semestre);
  }

  getHorarioPorDia(clase: any, dia: string): string {
    const horarioDia = clase.horarios.find((horario: any) => horario.dia === dia);
  
    if (horarioDia) {
      return `${horarioDia.horaInicio}\na\n${horarioDia.horaFin}`;
    } else {
      return '-';
    }
  }
  

  imprimirHorario() {
    const tablaHtml = this.tablaHorario.nativeElement.outerHTML;
    const ventanaImpresion = window.open('', '_blank');
    ventanaImpresion?.document.write('<html><head><title></title></head><body>');
    ventanaImpresion?.document.write('<h2>HORARIO</h2>');
    ventanaImpresion?.document.write('<style>');
    ventanaImpresion?.document.write(`
    body {
      font-family: Arial, sans-serif;
    }

  h2{
    text-align: center;
  }
    table {
      width: 100%;
      border-collapse: collapse;
      margin-bottom: 20px;
    }
  
    th, td {
      border: 1px solid #ddd;
      padding: 8px;
      text-align: left;
    }
  
    .fila-par {
      background-color: #f2f2f2;
    }
  
    .fila-impar {
      background-color: #fff;
    }
    `);
    ventanaImpresion?.document.write('</style>');
    ventanaImpresion?.document.write(tablaHtml);
    ventanaImpresion?.document.write('</body></html>');
    ventanaImpresion?.document.close();
    ventanaImpresion?.print();
  }
}
