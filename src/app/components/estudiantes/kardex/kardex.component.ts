import { Component, OnInit } from '@angular/core';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';

@Component({
  selector: 'app-kardex',
  templateUrl: './kardex.component.html',
  styleUrls: ['./kardex.component.css', 'kardex-querys.css']
})
export class KardexComponent implements OnInit {

  estudianteAutenticado: any;
  kardex: any;
  opcionSeleccionada: string = 'kardex';
  historialAcademico: any[] = [];
  currentMode: boolean = false;

  constructor(private estudiantesService: EstudiantesService) { }

  ngOnInit(): void {
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';

    this.estudianteAutenticado = this.estudiantesService.getEstudianteAutenticado();

    if (this.estudianteAutenticado) {
      this.kardex = this.estudiantesService.getKardexByEstudianteId(this.estudianteAutenticado.id);
      this.historialAcademico = this.estudiantesService.getHistorialAcademicoByEstudianteId(this.estudianteAutenticado.id);
    }
  }

  calculateSemesterAverage(materias: any[]): number {
    const totalCalificaciones = materias.reduce((total, materia) => {
      return total + ((materia.periodo1 + materia.periodo2 + materia.periodo3) / 3);
    }, 0);
  
    return totalCalificaciones / materias.length;
  }

  calculateOverallAverage(historialAcademico: any[]): number {
    const totalSemestres = historialAcademico.length;
    const totalCalificaciones = historialAcademico.reduce((total, semestre) => {
      return total + this.calculateSemesterAverage(semestre.materias);
    }, 0);
  
    return totalCalificaciones / totalSemestres;
  }

  imprimirHistorialAcademico() {
    const ventana = window.open('', '', 'width=600,height=600');
    if (ventana) {
      ventana.document.open();
      ventana.document.write('<html><head><title>Historial Académico</title>');
      ventana.document.write('</head><body>');

      ventana.document.write('<style>');
      ventana.document.write(`
      body {
        font-family: Arial, sans-serif;
      }

      .datos-estudiante,
      .kardex {
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 20px;
        font-size: 15px;
        page-break-inside: avoid; /* Evita que la tabla se divida entre páginas */
      }

      th, td {
        border: 1px solid #dddddd;
        text-align: left;
      }

      .fila-par {
        background-color: #f2f2f2;
      }

      .fila-impar {
        background-color: #ffffff;
      }
    `);
      ventana.document.write('</style>');
      // Imprimir datos del estudiante
      const datosEstudiante = document.getElementById('datosEstudiantes');
      if (datosEstudiante) {
        ventana.document.write('<section class="page-break">');
        ventana.document.write(datosEstudiante.outerHTML);
        ventana.document.write('</section>');
      }

      // Imprimir kardex o historial académico según la opción seleccionada
      if (this.opcionSeleccionada === 'kardex' && this.kardex) {
        const kardexSection = document.getElementById('kardex');
        if (kardexSection) {
          ventana.document.write('<section class="calificaciones-section">');
          ventana.document.write(kardexSection.outerHTML);
          ventana.document.write('</section>');
        }
      } else if (this.opcionSeleccionada === 'historialCompleto' && this.historialAcademico) {
        const historialAcademico = document.getElementById('historialAcademico');
        if (historialAcademico) {
          ventana.document.write('<section class="calificaciones-section">');
          ventana.document.write(historialAcademico.outerHTML);
          ventana.document.write('</section>');
        }
      }

      ventana.document.write('</body></html>');
      ventana.document.close();
      ventana.print();
    }
  }
}
