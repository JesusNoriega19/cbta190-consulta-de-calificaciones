import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { ZoomService } from '../../../services/zoom/zoom.service';
import { AlertasService } from '../../../services/alertas/alertas.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', 'login-querys.css']
})
export class LoginComponent implements OnInit {
  currentMode: boolean = false;
  username: string = '';
  password: string = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private zoomService: ZoomService,
    private alertasService: AlertasService
  ) {}

  ngOnInit(): void {
    this.zoomService.disableZoomOnMobile();
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  login() {
    // Validar que ambos campos no estén vacíos
    if (!this.username && !this.password) {
      this.alertasService.mostrarAlertaAdvertencia('Por favor, ingresa tu usuario y contraseña.');
      return;
    }

    // Validar que el campo de nombre de usuario no esté vacío
    if (!this.username) {
      this.alertasService.mostrarAlertaAdvertencia('Por favor, ingresa tu usuario.');
      return;
    }

    // Validar que el campo de contraseña no esté vacío
    if (!this.password) {
      this.alertasService.mostrarAlertaAdvertencia('Por favor, ingresa tu contraseña.');
      return;
    }

    // Realizar el inicio de sesión
    const isAuthenticated = this.authService.login(this.username, this.password);

    if (isAuthenticated) {
      this.router.navigate(['estudiantes/inicio']);
    } else {
      this.alertasService.mostrarAlertaError('Credenciales incorrectas');
    }
  }
}
