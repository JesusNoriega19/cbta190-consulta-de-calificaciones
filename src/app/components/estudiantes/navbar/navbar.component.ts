
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css', 'navbar-querys.css']
})
export class NavbarComponent {
  constructor( private authService: AuthService, private estudiantesService: EstudiantesService, private router: Router ) {}

  // Método de cierre de sesión
  logout() {
    this.authService.logout();
    this.router.navigate(['estudiantes/inicio-de-sesion']); // Redirige al usuario a la página de inicio de sesión
  }

  // Método para verificar si el usuario está autenticado
  isLoggedIn() {
    return this.authService.getIsLoggedIn();
  }

  // Método para obtener el nombre de usuario del estudiante autenticado
  getUsername() {
    return this.estudiantesService.getEstudianteAutenticado()?.username;
  }
}


