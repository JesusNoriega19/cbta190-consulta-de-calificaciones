import { Component, OnInit} from '@angular/core';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', 'profile-querys.css']
})
export class ProfileComponent implements OnInit {

  estudianteAutenticado: any; // Define una propiedad para almacenar el estudiante autenticado

  currentMode: boolean = false; 

  constructor(private estudiantesService: EstudiantesService) { }

  ngOnInit(): void {
    //dark mode
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';

    // Cuando el componente se inicializa, obtén el estudiante autenticado
    this.estudianteAutenticado = this.estudiantesService.getEstudianteAutenticado();
    console.log(this.estudianteAutenticado);
  }

//dark mode
  toggleColors() {
    const currentMode = localStorage.getItem('darkMode') == 'true';
    localStorage.setItem('darkMode', currentMode ? 'false' : 'true');
    
    setTimeout(() => {
      window.location.reload();
    }, 100);
  }
  
}
