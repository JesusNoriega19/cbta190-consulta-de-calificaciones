import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirtrabajosComponent } from './subirtrabajos.component';

describe('SubirtrabajosComponent', () => {
  let component: SubirtrabajosComponent;
  let fixture: ComponentFixture<SubirtrabajosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubirtrabajosComponent]
    });
    fixture = TestBed.createComponent(SubirtrabajosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
