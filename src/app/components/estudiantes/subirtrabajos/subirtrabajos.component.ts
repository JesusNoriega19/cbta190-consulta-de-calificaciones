// subirtrabajos.component.ts
import { Component, OnInit } from '@angular/core';
import { TrabajosService } from '../../../services/subir-trabajos/subir-trabajos.service';
import { Trabajo } from '../../../interfaces/subir-trabajos/subir-trabajos.interface';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';
import { AlertasService } from '../../../services/alertas/alertas.service';
import { ZoomService } from '../../../services/zoom/zoom.service';

@Component({
  selector: 'app-subirtrabajos',
  templateUrl: './subirtrabajos.component.html',
  styleUrls: ['./subirtrabajos.component.css', 'subirtrabajos-querys.css','subirtrabajos-querys-mobile.css'],
})
export class SubirtrabajosComponent implements OnInit {
  periodoSeleccionado: number = 1;
  trabajos: Trabajo[] = [];
  estudiante: any;
  archivosSubidos: { [key: string]: boolean } = {};
  mostrarRetroalimentacion: { [key: string]: boolean } = {};
  mostrarComentarios: { [key: string]: boolean } = {};
  mostrarIconoRetroalimentacion: { [key: string]: boolean } = {};
  mostrarIconoEstudiante: { [key: string]: boolean } = {};
  estadoArchivoSubido: { [key: string]: boolean } = {};
  currentMode: boolean = false;


  constructor(
    private trabajosService: TrabajosService,
    private estudiantesService: EstudiantesService,
    private alertasService: AlertasService,
    private zoomService: ZoomService,

  ) { }

  ngOnInit(): void {
    this.zoomService.disableZoomOnMobile();
    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
    this.estudiante = this.estudiantesService.getEstudianteAutenticado();
    if (this.estudiante) {
      this.cargarTrabajos(this.estudiante.id);
    }
  }

  cargarTrabajos(estudianteId: number) {
    this.trabajosService.getTrabajos(estudianteId).subscribe((trabajos) => (this.trabajos = trabajos));
  }

  handleFileInput(trabajo: Trabajo, periodo: number, event: any): void {
    const archivoInput = event.target as HTMLInputElement;
    const nuevoArchivo = archivoInput?.files?.[0];
    if (nuevoArchivo !== null && nuevoArchivo !== undefined) {
      if (!this.estadoArchivoSubido[`${trabajo.materia}-${periodo}`]) {
        trabajo.periodos[periodo - 1].archivo = nuevoArchivo;
        this.archivosSubidos[`${trabajo.materia}-${periodo}`] = false;
      } else {
        this.alertasService.mostrarAlertaAdvertencia('Ya hay un archivo subido. Elimina el archivo existente.');
        setTimeout(() => {
          archivoInput.value = '';
        }, 50);
        return; 
      }
      const label = document.getElementById(`labelArchivo-${trabajo.materia}-${periodo}`);
      if (label) {
        label.innerText = nuevoArchivo.name ?? 'Seleccionar archivo';
      }
    }
    setTimeout(() => {
      archivoInput.value = '';
    }, 100);
  }


  subirArchivo(trabajo: Trabajo, periodo: number): void {
    const archivo = trabajo.periodos[periodo - 1].archivo;
    if (archivo !== null && archivo !== undefined) {
      if (!this.estadoArchivoSubido[`${trabajo.materia}-${periodo}`]) {
        this.trabajosService.subirArchivo(trabajo, periodo, archivo).subscribe((exito) => {
          if (exito) {
            this.alertasService.mostrarAlertaExito('Archivo subido exitosamente.');
            this.archivosSubidos[`${trabajo.materia}-${periodo}`] = true;
            this.estadoArchivoSubido[`${trabajo.materia}-${periodo}`] = true;
            const inputArchivo = document.getElementById(`inputArchivo-${trabajo.materia}-${periodo}`) as HTMLInputElement;
            if (inputArchivo) {
              inputArchivo.value = '';
            }
            const label = document.getElementById(`labelArchivo-${trabajo.materia}-${periodo}`);
            if (label) {
              label.innerText = 'Seleccionar archivo';
            }
          } else {
            this.alertasService.mostrarAlertaError('Error al subir el archivo.');
          }
        });
      } else {
        this.alertasService.mostrarAlertaError('No se ha seleccionado un archivo.');
        setTimeout(() => {
          const inputArchivo = document.getElementById(`inputArchivo-${trabajo.materia}-${periodo}`) as HTMLInputElement;
          if (inputArchivo) {
            inputArchivo.value = '';
          }
        }, 50);
      }
    } else {
      console.error('No se ha seleccionado un archivo.');
      this.alertasService.mostrarAlertaError('No se ha seleccionado un archivo.');
    }
  }

  descargarArchivo(trabajo: Trabajo, periodo: number): void {
    const archivo = trabajo.periodos[periodo - 1].archivo;

    if (archivo !== null && archivo !== undefined) {
      const url = URL.createObjectURL(archivo);

      const enlace = document.createElement('a');
      enlace.href = url;
      enlace.download = archivo.name;
      document.body.appendChild(enlace);

      enlace.click();

      // Limpiar el objeto URL y remover el elemento del cuerpo después de la simulación de clic
      URL.revokeObjectURL(url);
      document.body.removeChild(enlace);
    } else {
      console.error('No hay archivo para descargar.');
    }
  }

  subirComentario(trabajo: Trabajo, periodo: number): void {
    // Lógica para subir comentario, si es necesario
    console.log('Comentario subido:', trabajo.periodos[periodo - 1].comentariosAlumno);
  }

  onPeriodoChange() {
    this.cargarTrabajos(this.estudiante.id);
  }

  toggleRetroalimentacion(trabajo: Trabajo, periodo: number): void {
    const key = `${trabajo.materia}-${periodo}`;
    this.mostrarRetroalimentacion[key] = !this.mostrarRetroalimentacion[key];
    this.mostrarIconoRetroalimentacion[key] = !this.mostrarIconoRetroalimentacion[key];
  }

  toggleComentarios(trabajo: Trabajo, periodo: number): void {
    const key = `${trabajo.materia}-${periodo}`;
    this.mostrarComentarios[key] = !this.mostrarComentarios[key];
    this.mostrarIconoEstudiante[key] = !this.mostrarIconoEstudiante[key];
  }

  eliminarArchivo(trabajo: Trabajo, periodo: number): void {
    this.alertasService.mostrarAlertaConfirmacion().then((result) => {
      if (result.isConfirmed) {
        trabajo.periodos[periodo - 1].archivo = null;
        this.archivosSubidos[`${trabajo.materia}-${periodo}`] = false;
        this.estadoArchivoSubido[`${trabajo.materia}-${periodo}`] = false; // Asegúrate de actualizar el estado

        this.alertasService.mostrarAlertaExito('Archivo eliminado exitosamente.');
      }
    });
  }
}
