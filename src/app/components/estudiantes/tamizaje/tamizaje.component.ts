// tamizaje.component.ts

import { Component, OnInit } from '@angular/core';
import { TamizajeService } from '../../../services/tamizaje/tamizaje.service';
import { PreguntaTamizaje } from '../../../interfaces/tamizaje/tamizaje.interface';
import { EstudiantesService } from '../../../services/estudiantes/estudiantes.service';
import { AlertasService } from '../../../services/alertas/alertas.service';

@Component({
  selector: 'app-tamizaje',
  templateUrl: './tamizaje.component.html',
  styleUrls: ['./tamizaje.component.css', 'tamizaje-querys.css', 'tamizaje-querys-mobile.css']
})
export class TamizajeComponent implements OnInit {
  preguntas: PreguntaTamizaje[] = [];
  preguntaActual: number = 0;
  respuestas: Map<number, string> = new Map();
  tamizajeTerminado: boolean = false;
  mostrarInstrucciones: boolean = false;
  currentMode: boolean = false;
  idEstudiante: number | undefined;

  constructor(private tamizajeService: TamizajeService, private estudiantesService: EstudiantesService, private alertasService: AlertasService) {}

  ngOnInit(): void {
    
    const estudianteAutenticado = this.estudiantesService.getEstudianteAutenticado();
    this.idEstudiante = estudianteAutenticado?.id;
    this.tamizajeTerminado = this.tamizajeService.verificarTamizajeRealizado(this.idEstudiante);

    if (!this.tamizajeTerminado) {
      this.preguntas = this.tamizajeService.getPreguntas();
    }

    const storedMode = localStorage.getItem('darkMode');
    this.currentMode = storedMode === 'true';
  }

  iniciarTamizaje() {
    this.mostrarInstrucciones = true;
  }

  seleccionarRespuesta(preguntaId: number, respuesta: string) {
    this.respuestas.set(preguntaId, respuesta);
  }

  cambiarPregunta() {
    const preguntasRestantes = this.preguntas.slice(this.preguntaActual, this.preguntaActual + 5);
  
    // Verifica si todas las preguntas actuales están contestadas
    if (preguntasRestantes.every(pregunta => this.respuestas.has(pregunta.id))) {
      // Puedes cambiar la lógica de navegación aquí
      this.preguntaActual += 5;
    } else {
      // Muestra un mensaje al usuario o realiza alguna acción
      this.alertasService.mostrarAlertaAdvertencia('Debes responder todas las preguntas antes de continuar.');
      console.log('Debes responder todas las preguntas antes de continuar.');
    }
  }
  
  finalizarTamizaje() {
    const todasLasPreguntasContestadas = this.preguntas.every(pregunta => this.respuestas.has(pregunta.id));
  
    if (todasLasPreguntasContestadas) {
      this.tamizajeTerminado = true;
      this.guardarRespuestas();
    } else {
      // Muestra un mensaje al usuario o realiza alguna acción
      this.alertasService.mostrarAlertaAdvertencia('Debes responder todas las preguntas antes de terminar.');
      console.log('Debes responder todas las preguntas antes de terminar.');
    }
  }
  

  guardarRespuestas() {
    if (this.respuestas.size > 0 && this.idEstudiante !== undefined) {
      this.tamizajeService.guardarRespuestas(this.idEstudiante, this.respuestas);
      localStorage.setItem('tamizajeRealizado', 'true');
    }
  }

  cancelarTamizaje() {
    this.mostrarInstrucciones = false;
    this.preguntaActual = 0;
    this.respuestas.clear();
  }
}
