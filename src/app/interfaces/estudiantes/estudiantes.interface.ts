// Archivo: estudiante.interface.ts
import { Kardex } from './kardex.interface';
import { HistorialAcademico } from './historialAcademico.interface';
import { horarioEstudiante } from './horarioEstudiante.interface';  // Agregamos la interfaz de Horario
import { GuiaEstudio } from './../guias-trabajo/guias-trabajo.interface';
import { Trabajo  } from './../subir-trabajos/subir-trabajos.interface';

export interface Estudiante {
  id: number;
  username: string;
  nombre: string;
  carrera: string;
  semestre: number;
  grupo: string;
  nocontrol: number;
  curp: string;
  kardex: Kardex[];
  historialAcademico: HistorialAcademico[];
  horario: horarioEstudiante[];  // Nueva propiedad para el Horario
  guiasEstudio: GuiaEstudio[];
  trabajos: Trabajo[];
}
