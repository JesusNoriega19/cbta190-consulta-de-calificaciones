// Archivo: horario.interface.ts


// Archivo: horarioEstudiante.interface.ts
export interface horarioEstudiante {
    id: number;
    estudianteId: number;
    semestre: number;
    clases: Clase[];
  }
  
  export interface Clase {
    materia: string;
    profesor: string;
    horarios: Horario[];
  }
  
  export interface Horario {
    dia: string;
    horaInicio: string;
    horaFin: string;
    salon: string;
  }
  