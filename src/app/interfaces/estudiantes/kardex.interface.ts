

export interface Kardex {
  id: number;
  semestre: number;
  materias: {
    nombre: string;
    profesor: string;
    periodo1: number;
    periodo2: number;
    periodo3: number;
  }[];
}
