// evaluacion-docente.interface.ts

export interface Pregunta {
  fechaPeriodo: string;
  texto: string;
  materias: Materia[];
  opcionesRespuesta: string[];
}

export interface Materia {
  idEstudiante: number;
  nombre: string;
  profesor: string;
}
