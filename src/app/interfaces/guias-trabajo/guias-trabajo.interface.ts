// interfaces/guias-trabajo.interface.ts
import { Estudiante } from '../../interfaces/estudiantes/estudiantes.interface';

export interface GuiaEstudio {
  asignatura: string;
  profesor: string;
  periodos: { [key: number]: string };
  estudiante?: Estudiante;
}
