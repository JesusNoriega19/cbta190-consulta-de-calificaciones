import { Estudiante } from "../estudiantes/estudiantes.interface";

export interface Trabajo {
  materia: string;
  profesor: string;
  periodos: {
    periodo: number;
    archivo: File | null; // Ahora es un archivo real o nulo
    comentariosAlumno: string;
    retroalimentacionDocente: string;
  }[];
  estudiante?: Estudiante;
}