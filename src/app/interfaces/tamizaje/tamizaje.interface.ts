// tamizaje.interface.ts

export interface PreguntaTamizaje {
    id: number;
    texto: string;
  }
  
  export interface RespuestasTamizaje {
    [preguntaId: number]: string;
  }
  