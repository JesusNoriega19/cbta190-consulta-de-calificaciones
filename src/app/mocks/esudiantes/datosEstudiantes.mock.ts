// import { Estudiante } from '../interfaces/estudiantes.interface';
import { Estudiante } from '../../interfaces/estudiantes/estudiantes.interface';
export const estudiantesMock: Estudiante[] = [

  { 
    id: 1, 
    username: '18117011900037', 
    nombre: 'DELGADO CAMPOS ELFEGO', 
    carrera: 'TÉCNICO AGROPECUARIO', 
    semestre: 6,
    grupo: '6A-AGR-2018-2021',
    nocontrol: 18117011900037, 
    curp: 'DECE030419HGRLMLA2',
    kardex: [],
    historialAcademico: [],
    horario: [],
    guiasEstudio:[],
    trabajos:[]
  },

  // { 
  //   id: 1, 
  //   username: '1', 
  //   nombre: 'Jesus Noriega Gutierrez', 
  //   carrera: 'Técnico en electrónica', 
  //   semestre: 3,
  //   grupo: '6A-AGR-2018-2021',
  //   nocontrol: 18117011900037, 
  //   curp: 'DECE030419HGRLMLA2',
  //   kardex: [],
  //   historialAcademico: [],
  //   horario: [],
  //   guiasEstudio:[],
  //   trabajos:[]
  // },
  {   
    id: 2, 
    username: '18117011900035', 
    nombre: 'DELGADO CAMPOS ELFEGO', 
    carrera: 'TÉCNICO AGROPECUARIO', 
    semestre: 6,
    grupo: '6A-AGR-2018-2021',
    nocontrol: 18117011900035, 
    curp: 'DECE030419HGRLMLA2',
    kardex: [],
    historialAcademico: [],
    horario: [],
    guiasEstudio:[],
    trabajos:[]
  },
];



