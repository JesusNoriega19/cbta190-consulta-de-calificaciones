// Archivo: horario.mock.ts
import { horarioEstudiante } from '../../interfaces/estudiantes/horarioEstudiante.interface';

export const horarioMock: horarioEstudiante[] = [
  {
    id: 1,
    estudianteId: 1,
    semestre: 6,
    clases: [
      {
        "materia": "TUTORIA 6",
        "profesor": "LIC. ARAGON JIMENEZ ESTHER",
        "horarios": [
          { dia: "Lunes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" }
        ],
      },
      {
        "materia": "MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE",
        "profesor": "LIC. ARAGON JIMENEZ ESTHER",
        "horarios": [
          { dia: "Lunes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Martes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" }
        ],
      },
      {
        "materia": "SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO",
        "profesor": "ING SANCHEZ ALVEAR REYES",
        "horarios": [
          { dia: "Lunes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Martes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" }
        ],
      },
      {
        "materia": "SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO",
        "profesor": "ING. VALENCIA RUIZ REBECA",
        "horarios": [
          { dia: "Lunes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Martes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TEMAS DE FILOSOFIA",
        "profesor": "LIC DOMINGUEZ PALACIOS GUILLERMO",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TALLER DE MATEMATICAS 6",
        "profesor": "GUTIERREZ SANTIAGO SERGIO HUMBERTO",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TALLER DE LECTURA 6",
        "profesor": "PONCE ALVARADO HILDEGAR",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "PROBABILIDAD Y ESTADÍSTICA",
        "profesor": "ING. VALERA JUAREZ MARIA VANESSA",
        "horarios": [
          { dia: "Lunes", horaInicio: "13:00", horaFin: "14:00", salon: "A101" },
          { dia: "Martes", horaInicio:"13:00", horaFin: "14:00", salon: "A101" },
          { dia: "Miércoles", horaInicio:"13:00", horaFin: "14:00", salon: "A101" },
          { dia: "Jueves", horaInicio:"13:00", horaFin: "14:00", salon: "A101" },
          { dia: "Viernes", horaInicio:"13:00", horaFin: "14:00", salon: "A101" }
        ],
      },
    ],
  },

  {
    id: 2,
    estudianteId: 2,
    semestre: 3,
    clases: [
      {
        "materia": "PROBABILIDAD Y ESTADÍSTICA",
        "profesor": "ING. VALERA JUAREZ MARIA VANESSA",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" }
        ],
      },
      {
        "materia": "TUTORIA 6",
        "profesor": "LIC. ARAGON JIMENEZ ESTHER",
        "horarios": [
          { dia: "Lunes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" }
        ],
      },
      {
        "materia": "MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE",
        "profesor": "LIC. ARAGON JIMENEZ ESTHER",
        "horarios": [
          { dia: "Lunes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Martes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "09:00", horaFin: "10:00", salon: "A101" }
        ],
      },
      {
        "materia": "SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO",
        "profesor": "ING SANCHEZ ALVEAR REYES",
        "horarios": [
          { dia: "Lunes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Martes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "10:00", horaFin: "11:00", salon: "A101" }
        ],
      },
      {
        "materia": "SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO",
        "profesor": "ING. VALENCIA RUIZ REBECA",
        "horarios": [
          { dia: "Lunes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Martes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "11:00", horaFin: "12:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TEMAS DE FILOSOFIA",
        "profesor": "LIC DOMINGUEZ PALACIOS GUILLERMO",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TALLER DE MATEMATICAS 6",
        "profesor": "GUTIERREZ SANTIAGO SERGIO HUMBERTO",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
      {
        "materia": "TALLER DE LECTURA 6",
        "profesor": "PONCE ALVARADO HILDEGAR",
        "horarios": [
          { dia: "Lunes", horaInicio: "07:00", horaFin: "08:00", salon: "A101" },
          { dia: "Martes", horaInicio: "08:00", horaFin: "09:00", salon: "A101" },
          { dia: "Miércoles", horaInicio: "09:00", horaFin: "10:00", salon: "A101" },
          { dia: "Jueves", horaInicio: "10:00", horaFin: "11:00", salon: "A101" },
          { dia: "Viernes", horaInicio: "11:00", horaFin: "12:00", salon: "A101" }
        ],
      },
    ],
  },
  // ... Otros horarios para diferentes semestres o estudiantes
];
