import { Kardex } from '../../interfaces/estudiantes/kardex.interface';

export const kardexMock: Kardex[] = [
  {
    id: 1,
    semestre: 6,
    materias: [
      {
        nombre: 'PROBABILIDAD Y ESTADISTÍCA',
        profesor: 'ING. VALERA JUAREZ MARIA VANESSA',
        periodo1: 7,
        periodo2: 8,
        periodo3: 8,
      },
      {
        nombre: 'TUTORIA 6',
        profesor: 'LIC. ARAGON JIMENEZ ESTHER',
        periodo1: 10,
        periodo2: 10,
        periodo3: 10,
      },
      {
        nombre: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE',
        profesor: 'LIC. ARAGON JIMENEZ ESTHER',
        periodo1: 8,
        periodo2: 8,
        periodo3: 9,
      },
      {
        nombre: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO',
        profesor: 'ING SANCHEZ ALVEAR REYES',
        periodo1: 6,
        periodo2: 6,
        periodo3: 6,
      },
      {
        nombre: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO',
        profesor: 'ING. VALENCIA RUIZ REBECA',
        periodo1: 9,
        periodo2: 8,
        periodo3: 8,
      },
      {
        nombre: 'TEMAS DE FILOSOFIA',
        profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO',
        periodo1: 10,
        periodo2: 10,
        periodo3: 9,
      },
      {
        nombre: 'TALLER DE MATEMATICAS 6',
        profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO',
        periodo1: 9,
        periodo2: 9,
        periodo3: 10,
      },
      {
        nombre: 'TALLER DE LECTURA 6',
        profesor: 'PONCE ALVARADO HILDEGAR',
        periodo1: 7,
        periodo2: 6,
        periodo3: 6,
      },
      
    ],
  },
];












