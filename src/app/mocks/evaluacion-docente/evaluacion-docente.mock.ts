// evaluacion-docente.mock.ts

import { Pregunta } from '../../interfaces/evaluacion-docente/evaluacion-docente.interface';

export const evaluacionDocenteMock: Pregunta[] = [
  // Pregunta 1
  {
    fechaPeriodo: 'Noviembre 2023',
    texto: '¿Cómo evalúas la calidad de las clases?',
    materias: [
      { idEstudiante: 1, nombre: 'PROBABILIDAD Y ESTADISTÍCA', profesor: 'ING. VALERA JUAREZ MARIA VANESSA '},
      { idEstudiante: 1, nombre: 'TUTORIA 6', profesor: 'LIC. ARAGON JIMENEZ ESTHER'},
      { idEstudiante: 1, nombre: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE', profesor: 'LIC. ARAGON JIMENEZ ESTHER' },
      { idEstudiante: 1, nombre: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO', profesor: 'ING SANCHEZ ALVEAR REYES ' },
      { idEstudiante: 1, nombre: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO', profesor: 'ING. VALENCIA RUIZ REBECA' },
      { idEstudiante: 1, nombre: 'TEMAS DE FILOSOFIA', profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO' },
      { idEstudiante: 1, nombre: 'TALLER DE MATEMATICAS 6', profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO ' },
      { idEstudiante: 1, nombre: 'TALLER DE LECTURA 6 ', profesor: 'PONCE ALVARADO HILDEGAR' },
    ],
    opcionesRespuesta: ['Muy buena', 'Buena', 'Regular', 'Mala', 'Muy mala'],
  },
  // Pregunta 2
  {
    fechaPeriodo: 'Noviembre 2023',
    texto: '¿Estás satisfecho con los materiales de estudio proporcionados?',
    materias: [
      { idEstudiante: 1, nombre: 'PROBABILIDAD Y ESTADISTÍCA', profesor: 'ING. VALERA JUAREZ MARIA VANESSA '},
      { idEstudiante: 1, nombre: 'TUTORIA 6', profesor: 'LIC. ARAGON JIMENEZ ESTHER'},
      { idEstudiante: 1, nombre: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE', profesor: 'LIC. ARAGON JIMENEZ ESTHER' },
      { idEstudiante: 1, nombre: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO', profesor: 'ING SANCHEZ ALVEAR REYES ' },
      { idEstudiante: 1, nombre: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO', profesor: 'ING. VALENCIA RUIZ REBECA' },
      { idEstudiante: 1, nombre: 'TEMAS DE FILOSOFIA', profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO' },
      { idEstudiante: 1, nombre: 'TALLER DE MATEMATICAS 6', profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO ' },
      { idEstudiante: 1, nombre: 'TALLER DE LECTURA 6 ', profesor: 'PONCE ALVARADO HILDEGAR' },
    ],
    opcionesRespuesta: ['Excelente', 'Buena', 'Regular', 'Mala', 'Muy mala'],
  },
  // Pregunta 3
  {
    fechaPeriodo: 'Noviembre 2023',
    texto: '¿Cómo calificarías la disponibilidad del profesor para responder preguntas fuera del horario de clases?',
    materias: [
      { idEstudiante: 1, nombre: 'PROBABILIDAD Y ESTADISTÍCA', profesor: 'ING. VALERA JUAREZ MARIA VANESSA '},
      { idEstudiante: 1, nombre: 'TUTORIA 6', profesor: 'LIC. ARAGON JIMENEZ ESTHER'},
      { idEstudiante: 1, nombre: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE', profesor: 'LIC. ARAGON JIMENEZ ESTHER' },
      { idEstudiante: 1, nombre: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO', profesor: 'ING SANCHEZ ALVEAR REYES ' },
      { idEstudiante: 1, nombre: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO', profesor: 'ING. VALENCIA RUIZ REBECA' },
      { idEstudiante: 1, nombre: 'TEMAS DE FILOSOFIA', profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO' },
      { idEstudiante: 1, nombre: 'TALLER DE MATEMATICAS 6', profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO ' },
      { idEstudiante: 1, nombre: 'TALLER DE LECTURA 6 ', profesor: 'PONCE ALVARADO HILDEGAR' },
    ],
    opcionesRespuesta: ['Excelente', 'Buena', 'Regular', 'Mala', 'Muy mala'],
  },
  // Pregunta 4
  {
    fechaPeriodo: 'Noviembre 2023',
    texto: '¿Recomendarías esta materia a tus compañeros?',
    materias: [
      { idEstudiante: 1, nombre: 'PROBABILIDAD Y ESTADISTÍCA', profesor: 'ING. VALERA JUAREZ MARIA VANESSA '},
      { idEstudiante: 1, nombre: 'TUTORIA 6', profesor: 'LIC. ARAGON JIMENEZ ESTHER'},
      { idEstudiante: 1, nombre: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE', profesor: 'LIC. ARAGON JIMENEZ ESTHER' },
      { idEstudiante: 1, nombre: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO', profesor: 'ING SANCHEZ ALVEAR REYES ' },
      { idEstudiante: 1, nombre: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO', profesor: 'ING. VALENCIA RUIZ REBECA' },
      { idEstudiante: 1, nombre: 'TEMAS DE FILOSOFIA', profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO' },
      { idEstudiante: 1, nombre: 'TALLER DE MATEMATICAS 6', profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO ' },
      { idEstudiante: 1, nombre: 'TALLER DE LECTURA 6 ', profesor: 'PONCE ALVARADO HILDEGAR' },
    ],
    opcionesRespuesta: ['Excelente', 'Buena', 'Regular', 'Mala', 'Muy mala'],
  },
  // Agrega más preguntas aquí
];
