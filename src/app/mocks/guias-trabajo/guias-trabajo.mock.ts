// guias-trabajo.mock.ts
import { GuiaEstudio } from '../../interfaces/guias-trabajo/guias-trabajo.interface';
import { estudiantesMock } from '../../mocks/esudiantes/datosEstudiantes.mock';

const guiasTrabajoMock: GuiaEstudio[] = [
  { asignatura: 'PROBABILIDAD Y ESTADÍSTICA', profesor: 'ING. VALERA JUAREZ MARIA VANESSA ', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'TUTORIA 6', profesor: 'LIC. ARAGON JIMENEZ ESTHER', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE', profesor: 'LIC. ARAGON JIMENEZ ESTHER', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO ', profesor: '	ING SANCHEZ ALVEAR REYES', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO ', profesor: 'ING. VALENCIA RUIZ REBECA ', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'TEMAS DE FILOSOFIA', profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'TALLER DE MATEMATICAS 6', profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },
  { asignatura: 'TALLER DE LECTURA 6', profesor: 'PONCE ALVARADO HILDEGAR', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[0] },


  // ESTUDIANTE 2

  { asignatura: 'Asignatura3', profesor: 'Profesor3', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[1] },
  { asignatura: 'Asignatura4', profesor: 'Profesor4', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[1] },
  { asignatura: 'Asignatura5', profesor: 'Profesor5', periodos: { 1: 'guia-periodo1.pdf', 2: 'guia-periodo2.pdf', 3: 'guia-periodo3.pdf' }, estudiante: estudiantesMock[2] },

];

export default guiasTrabajoMock;
