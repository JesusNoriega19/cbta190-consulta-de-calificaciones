// trabajos.mock.ts
import { Trabajo } from '../../interfaces/subir-trabajos/subir-trabajos.interface';
import { estudiantesMock } from '../../mocks/esudiantes/datosEstudiantes.mock'; 

const trabajosMock: Trabajo[] = [
  {
    materia: 'PROBABILIDAD Y ESTADÍSTICA',
    profesor: 'MARIA VANESSA VALERA',
    periodos: [
      {
        periodo: 1,
        archivo: null,
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE', 
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },
  {
    materia: 'TUTORIA 6',
    profesor: 'LIC. ARAGON JIMENEZ ESTHER',
    periodos: [
      {
        periodo: 1,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },

  {
    materia: 'MÓDULO: OPERA PROYECTOS DE DESARROLLO SUSTENTABLE',
    profesor: 'LIC. ARAGON JIMENEZ ESTHER',
    periodos: [
      {
        periodo: 1,
        archivo: null, 
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE',
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },

  {
    materia: 'SUBMODULO I ELABORA PERFIL DE PROYECTO AGROPECUARIO',
    profesor: 'ING SANCHEZ ALVEAR REYES',
    periodos: [
      {
        periodo: 1,
        archivo: null, 
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE', 
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },
  {
    materia: 'SUBMODULO II OPERA PROYECTO PRODUCTIVO AGROPECUARIO',
    profesor: 'ING. VALENCIA RUIZ REBECA',
    periodos: [
      {
        periodo: 1,
        archivo: null, 
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE', 
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },
  {
    materia: 'TEMAS DE FILOSOFIA',
    profesor: 'LIC DOMINGUEZ PALACIOS GUILLERMO',
    periodos: [
      {
        periodo: 1,
        archivo: null, 
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE', 
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },
  {
    materia: 'TALLER DE MATEMATICAS 6',
    profesor: 'GUTIERREZ SANTIAGO SERGIO HUMBERTO',
    periodos: [
      {
        periodo: 1,
        archivo: null, 
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE', 
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },

  {
    materia: 'TALLER DE LECTURA 6',
    profesor: 'PONCE ALVARADO HILDEGAR',
    periodos: [
      {
        periodo: 1,
        archivo: null,
        comentariosAlumno: '', 
        retroalimentacionDocente: 'EXCELENTE',
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[0],
  },



// Estudiante 2
  
  {
    materia: 'TUTORIA 6',
    profesor: 'ESTHER ARAGON',
    periodos: [
      {
        periodo: 1,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 2,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
      {
        periodo: 3,
        archivo: null,
        comentariosAlumno: '',
        retroalimentacionDocente: '',
      },
    ],
    estudiante: estudiantesMock[1],
  },
];

export default trabajosMock;
