import { PreguntaTamizaje } from '../../interfaces/tamizaje/tamizaje.interface';

export const tamizajeMock: PreguntaTamizaje[] = [
  { id: 1, texto: '1. ¿Te sientes motivado/a para participar activamente en las clases?' },
  { id: 2, texto: '2. ¿Consideras que los materiales de estudio son útiles para tu aprendizaje?' },
  { id: 3, texto: '3. ¿Tienes acceso a recursos tecnológicos para seguir las clases en línea?' },
  { id: 4, texto: '4. ¿Sientes que puedes expresar tus dudas y opiniones durante las clases?' },
  { id: 5, texto: '5. ¿El tiempo asignado para las tareas y actividades es suficiente?' },
  { id: 6, texto: '6. ¿Encuentras útiles las tutorías académicas?' },
  { id: 7, texto: '7. ¿Cómo calificarías el apoyo recibido por parte de los profesores?' },
  { id: 8, texto: '8. ¿Te sientes cómodo/a con la modalidad de clases en línea?' },
  { id: 9, texto: '9. ¿El contenido de las clases se adapta a tus necesidades de aprendizaje?' },
  { id: 10, texto: '10. ¿Cómo evalúas tu nivel de participación en actividades extracurriculares?' },
  { id: 11, texto: '11. ¿Consideras que las evaluaciones reflejan tu conocimiento adquirido?' },
  { id: 12, texto: '12. ¿Cómo calificarías la comunicación con tus compañeros de clase?' },
  { id: 13, texto: '13. ¿Sientes que recibes retroalimentación oportuna sobre tu desempeño académico?' },
  { id: 14, texto: '14. ¿Te sientes parte de la comunidad estudiantil?' },
  { id: 15, texto: '15. ¿Encuentras fácil acceder a recursos bibliográficos y de investigación?' },
  { id: 16, texto: '16. ¿Cómo calificarías el soporte técnico para las clases en línea?' },
  { id: 17, texto: '17. ¿Consideras que se promueve un ambiente inclusivo en tu institución?' },
  { id: 18, texto: '18. ¿Te sientes preparado/a para enfrentar los desafíos académicos?' },
  { id: 19, texto: '19. ¿Cómo calificarías el acceso a servicios de salud y bienestar?' },
  { id: 20, texto: '20. ¿Tienes alguna sugerencia para mejorar la experiencia estudiantil?' },
];
