import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon, SweetAlertOptions, SweetAlertResult } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {
  private darkMode: boolean = false;

  constructor() { }

  setDarkMode(isDark: boolean): void {
    this.darkMode = isDark;
  }

  private applyDarkModeStyles(options: SweetAlertOptions): SweetAlertOptions {
    if (this.darkMode) {
      // Aplica estilos oscuros a las opciones de Swal
      options.customClass = {
        icon:'custom-swal-icon',
        popup: 'custom-swal-container-dark',
        title: 'custom-swal-title-dark',
        actions: 'custom-swal-actions-dark',
        confirmButton: 'custom-swal-confirm-button-dark',
        cancelButton: 'custom-swal-cancel-button-dark'
      };
    }
    return options; 
  }

  mostrarAlertaConfirmacion(): Promise<SweetAlertResult> {
    const options: SweetAlertOptions = {
      title: '¿Estás seguro que deseas eliminar?',
      icon: 'warning',
      iconColor:'#F2CC0F',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar',
      customClass: {
        icon:'custom-swal-icon',
        popup: 'custom-swal-container',
        title: 'custom-swal-title',
        actions: 'custom-swal-actions',
        confirmButton: 'custom-swal-confirm-button',
        cancelButton: 'custom-swal-cancel-button'
      },
    };

    this.applyDarkModeStyles(options);

    return Swal.fire(options);
  }

  mostrarAlertaExito(mensaje: string = ''): void {
    const options: SweetAlertOptions = {
      title: mensaje,
      icon: 'success',
      timer: 2000,
      showConfirmButton: true,
      customClass: {
        icon:'custom-swal-icon',
        popup: 'custom-swal-container',
        title: 'custom-swal-title',
        actions: 'custom-swal-actions',
        confirmButton: 'custom-swal-confirm-button',
        cancelButton: 'custom-swal-cancel-button'
      },
    };

    this.applyDarkModeStyles(options);

    Swal.fire(options);
  }

  mostrarAlertaAdvertencia(mensaje: string = ''): void {
    const options: SweetAlertOptions = {
      title: mensaje,
      icon: 'warning',
      iconColor:'#F2CC0F',
      timer: 2000,
      showConfirmButton: true,
      customClass: {
        icon:'custom-swal-icon',
        popup: 'custom-swal-container',
        title: 'custom-swal-title',
        actions: 'custom-swal-actions',
        confirmButton: 'custom-swal-confirm-button',
        cancelButton: 'custom-swal-cancel-button'
      },
    };

    this.applyDarkModeStyles(options);

    Swal.fire(options);
  }

  mostrarAlertaError(mensaje: string = ''): void {
    const options: SweetAlertOptions = {
      title: mensaje,
      icon: 'error',
      timer: 2000,
      showConfirmButton: true,
      customClass: {
        icon:'custom-swal-icon',
        popup: 'custom-swal-container',
        title: 'custom-swal-title',
        actions: 'custom-swal-actions',
        confirmButton: 'custom-swal-confirm-button',
        cancelButton: 'custom-swal-cancel-button'
      },
    };

    this.applyDarkModeStyles(options);

    Swal.fire(options);
  }
}
