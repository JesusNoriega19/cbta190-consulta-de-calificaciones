import { Injectable } from '@angular/core';
import { authenticationData } from '../../mocks/esudiantes/auth.mock';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isLoggedIn: boolean = false;

  constructor() {
    const username = localStorage.getItem('username');
    this.isLoggedIn = !!username;
  }

  login(username: string, password: string): boolean {
    const user = authenticationData.find((user) => user.username === username && user.password === password);
  
    if (user) {
      this.isLoggedIn = true;
      localStorage.setItem('username', username);
    }
  
    return !!user;
  }

  logout() {
    this.isLoggedIn = false;
    localStorage.removeItem('username');
  }

  getIsLoggedIn() {
    return this.isLoggedIn;
  }
}
