import { Injectable } from '@angular/core';
import { estudiantesMock } from '../../mocks/esudiantes/datosEstudiantes.mock';
import { Kardex } from '../../interfaces/estudiantes/kardex.interface';
import { kardexMock } from '../../mocks/esudiantes/kardex.mock';
import { HistorialAcademico } from '../../interfaces/estudiantes/historialAcademico.interface';
import { historialMock } from '../../mocks/esudiantes/historialAcademico.mock';
import { horarioEstudiante } from '../../interfaces/estudiantes/horarioEstudiante.interface';
import { horarioMock } from '../../mocks/esudiantes/horarioEstudiante.mock';

@Injectable({
  providedIn: 'root',
})
export class EstudiantesService {

  getEstudianteByUsername(username: string) {
    return estudiantesMock.find((estudiante) => estudiante.username === username);
  }

  getEstudianteAutenticado() {
    const username = localStorage.getItem('username');
    if (username !== null) {
      return this.getEstudianteByUsername(username);
    } else {
      return null;
    }
  }
  
  getKardexByEstudianteId(id: number): Kardex | undefined {
    const estudiante = estudiantesMock.find((estudiante) => estudiante.id === id);
    const kardex = kardexMock.find((k) => k.semestre === estudiante?.semestre);
    return kardex;
  }

  getHistorialAcademicoByEstudianteId(id: number): HistorialAcademico[] {
    return historialMock.filter((historial) => historial.estudianteId === id);
  }

  getHorarioEstudianteAutenticado(): horarioEstudiante | undefined {
    const estudianteAutenticado = this.getEstudianteAutenticado();
    if (estudianteAutenticado) {
      return this.getHorarioByEstudianteId(estudianteAutenticado.id);
    }
    return undefined;
  }

  getHorarioByEstudianteId(id: number): horarioEstudiante | undefined {
    return horarioMock.find((horario) => horario.estudianteId === id);
  }

  

  
}
