// evaluacion-docente.service.ts

import { Injectable } from '@angular/core';
import { evaluacionDocenteMock } from '../../mocks/evaluacion-docente/evaluacion-docente.mock';
import { Pregunta } from '../../interfaces/evaluacion-docente/evaluacion-docente.interface';

@Injectable({
  providedIn: 'root',
})
export class EvaluacionDocenteService {
  getPreguntasPorEstudiante(idEstudiante: number): Pregunta[] {
    // Filtra las preguntas por el idEstudiante
    return evaluacionDocenteMock.filter((pregunta) =>
      pregunta.materias.some((materia) => materia.idEstudiante === idEstudiante)
    );
  }

  // Modifica el método para usar el idEstudiante
  guardarEstadoEvaluacion(idEstudiante: number) {
    localStorage.setItem(`evaluacionRealizada_${idEstudiante}`, 'true');
  }

  // Modifica el método para usar el idEstudiante
  verificarEvaluacionRealizada(idEstudiante: number): boolean {
    const evaluacionRealizada = localStorage.getItem(`evaluacionRealizada_${idEstudiante}`);
    return evaluacionRealizada === 'true';
  }
}
