import { TestBed } from '@angular/core/testing';

import { GuiasTrabajoService } from './guias-trabajo.service';

describe('GuiasTrabajoService', () => {
  let service: GuiasTrabajoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuiasTrabajoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
