// guias-trabajo.service.ts
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GuiaEstudio } from '../../interfaces/guias-trabajo/guias-trabajo.interface';
import guiasTrabajoMock from '../../mocks/guias-trabajo/guias-trabajo.mock'; // Importa el mock de guías de trabajo

@Injectable({
  providedIn: 'root',
})
export class GuiasTrabajoService {
  getGuiasEstudio(estudianteId: number | null): Observable<GuiaEstudio[]> {
    // Filtra las guías de trabajo según el estudianteId
    const guiasFiltradas = guiasTrabajoMock.filter((guia) => !estudianteId || guia.estudiante?.id === estudianteId);
    return of(guiasFiltradas);
  }

  // Puedes agregar más métodos según sea necesario

  constructor() {}
}
  