import { TestBed } from '@angular/core/testing';

import { TrabajosService } from './subir-trabajos.service';

describe('SubirTrabajosService', () => {
  let service: TrabajosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrabajosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
