// trabajos.service.ts
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Trabajo } from '../../interfaces/subir-trabajos/subir-trabajos.interface';
import trabajosMock from '../../mocks/subir-trabajos/subir-trabajos.mock'; // Importa el mock de trabajos

@Injectable({
  providedIn: 'root',
})
export class TrabajosService {
  getTrabajos(estudianteId: number): Observable<Trabajo[]> {
    const trabajosFiltrados = trabajosMock.filter(t => t.estudiante?.id === estudianteId);
    return of(trabajosFiltrados);
  }

 
  subirArchivo(trabajo: Trabajo, periodo: number, archivo: File): Observable<boolean> {
    const nuevoTrabajo = { ...trabajo };
    nuevoTrabajo.periodos[periodo - 1].archivo = archivo;
    const index = trabajosMock.findIndex(t => t.materia === trabajo.materia);
    if (index !== -1) {
      trabajosMock[index] = nuevoTrabajo;
    }
    return of(true);
  }

  eliminarArchivo(trabajo: Trabajo, periodo: number): Observable<boolean> {
    trabajo.periodos[periodo - 1].archivo = null;
    return of(true);
  }
  constructor() {}
}
