import { TestBed } from '@angular/core/testing';

import { TamizajeService } from './tamizaje.service';

describe('TamizajeService', () => {
  let service: TamizajeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TamizajeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
