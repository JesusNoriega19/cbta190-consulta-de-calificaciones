// tamizaje.service.ts

import { Injectable } from '@angular/core';
import { tamizajeMock } from '../../mocks/tamizaje/tamizaje.mock';
import { PreguntaTamizaje, RespuestasTamizaje } from '../../interfaces/tamizaje/tamizaje.interface';

@Injectable({
  providedIn: 'root',
})
export class TamizajeService {
  getPreguntas(): PreguntaTamizaje[] {
    return tamizajeMock;
  }

  verificarTamizajeRealizado(idEstudiante: number | undefined): boolean {
    if (idEstudiante !== undefined) {
      const tamizajeRealizado = localStorage.getItem(`tamizajeRealizado_${idEstudiante}`);
      return tamizajeRealizado === 'true';
    }
    return false;
  }

  guardarRespuestas(idEstudiante: number, respuestas: Map<number, string>) {
    // Lógica para guardar las respuestas del tamizaje asociadas al estudiante
    const respuestasTamizaje: RespuestasTamizaje = {};
    respuestas.forEach((valor, preguntaId) => {
      respuestasTamizaje[preguntaId] = valor;
    });
    console.log(`Respuestas del tamizaje para el estudiante ${idEstudiante}:`, respuestasTamizaje);
    // Aquí puedes realizar operaciones de guardado, como enviar las respuestas al servidor, etc.
    localStorage.setItem(`tamizajeRealizado_${idEstudiante}`, 'true');
  }
}
